from selenium.webdriver.support.ui import Select
from types import NoneType
from numpy import equal
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

observation_width = 670
observation_height = 947
element_to_wait_on_for_process_launch = 'dumyr'
service = Service(ChromeDriverManager().install())
chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument(
    f'window-size={observation_width},{observation_height}')
driver = webdriver.Chrome(service=service, options=chrome_options)

# https://www.offerzen.com/users/sign_in
# https://www.facebook.com/
# https://www.google.com/
# "https://zoom.us/signin"
# https://swift3.valueactive.eu/Swift.StateMachine/swiftlaunch/uilauncher

driver.get("https://swift3.valueactive.eu/Swift.StateMachine/swiftlaunch/uilauncher")

fields = driver.find_elements(By.CSS_SELECTOR, "*")

for element in fields:
    attribute = element.get_dom_attribute('type')
    if type(attribute) != NoneType and attribute != '':

        if attribute == 'text' or attribute == 'email':
            try:
                element.send_keys('test@test.co.za')
                print(element.location)
            except BaseException as error:
                print(attribute)

        elif attribute == 'password':
            element.send_keys('pasword404')

        elif attribute == 'checkbox':
            element.click()

        elif attribute == 'radio':
            element.click()

        elif attribute == 'button':
             print('Button')
             print(element.location)

        elif attribute == 'number':
            element.clear()
            element.send_keys(2005)
        else:
            pass

print(driver.title)
