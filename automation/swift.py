from multiprocessing.connection import wait
from numpy import number
from selenium.webdriver.support.ui import Select
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from types import NoneType
from enum import Enum
import random
from selenium.webdriver.remote.webelement import WebElement


class ElementActions(Enum):
    Click = 0
    FillWithValidText = 1
    FillWithInvalidText = 2


element_to_wait_on_for_process_launch = 'page-banner-v-01_title'
service = Service(ChromeDriverManager().install())

observation_width = 670
observation_height = 947
field_position = {'x': 90, 'y': 311}
server_id_field = {'x': 90, 'y': 451}
button_position = {'x': 90, 'y': 980}
process_position = {'x': 90, 'y': 81}


def get_driver():
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument(
        f'window-size={observation_width},{observation_height}')

    return webdriver.Chrome(service=service, options=chrome_options)


driver = get_driver()
driver.get(
    "https://swift3.valueactive.eu/Swift.StateMachine/swiftlaunch/uilauncher")


def get_mgs_parent(element: WebElement):
    script = '''
    get_mgs_parent = function(e){
        p = e.parentNode

        if (p.tagName.toLowerCase().indexOf('mgs-') > -1){
            return p
        }

        return get_mgs_parent(p);
    }
    return get_mgs_parent(arguments[0]);
    '''
    return driver.execute_script(script, element)


def include_regex_js_lib():
    regex_js_lib_filename = 'randexp.min.js'

    with open(f'./{regex_js_lib_filename}') as f:
        content = f.read()

    driver.execute_script(content)
    driver.execute_script(
        'generated = []; Object.keys(swift.MGSRegExMap.patterns).forEach(k => generated[k] = (new RandExp(swift.MGSRegExMap.patterns[k]).gen()))')


def fill_text(input_element: WebElement, text):
    input_element.clear()
    input_element.send_keys(text)


def select_option(drop_down_list_element: WebElement, option_index: number):
    select = Select(drop_down_list_element)
    select.select_by_index(option_index)


def get_element_by_position(x, y):
    return driver.execute_script('return document.elementFromPoint(arguments[0], arguments[1]);', x, y)


def launch_registration():
    element = get_element_by_position(field_position['x'], field_position['y'])
    server_id = get_element_by_position(
        server_id_field['x'], server_id_field['y'])
    procces_name = get_element_by_position(
        process_position['x'], process_position['y'])
    submit_btn = get_element_by_position(
        button_position['x'], button_position['y'])
    submit_btn = driver.find_element(By.ID, 'Submit')

    fill_text(input_element=element, text='Test data')
    fill_text(input_element=server_id, text=3927)
    select_option(procces_name, 22)
    submit_btn.click()


def get_component_data(element):
    variant_data = driver.execute_script(
        'return angular.element(document.querySelector("[mgs-swift-ui2]")).scope().data')
    element_data = variant_data.keys()
    mgs_element = get_mgs_parent(element)
    data_key = mgs_element.get_attribute('data').replace('data.', '')

    for key in element_data:
        if 'regExClass' in variant_data[key]:
            regex_class_name = variant_data[key]['regExClass']
            generated_from_regex = driver.execute_script(
                f'return generated["{regex_class_name}"]')
            variant_data[key]['regExGenerated'] = generated_from_regex

        if data_key not in variant_data:
            print(f'Variant data did not contain a key of "{data_key}".')
            return None

    return variant_data[data_key]


def get_text_from_validation_requirements(component_data, should_meet_requirements):
    if component_data == None:
        return ''

    if 'regExGenerated' in component_data:
        generated = component_data['regExGenerated']
        min_length = int(component_data['minLength']['value'])
        max_length = int(component_data['maxLength']['value'])

        # If the min and max lengths are correct.
        if len(generated) >= min_length and len(generated) <= max_length:
            # If we were looking for a valid response, return the already-valid response.
            if should_meet_requirements:
                return generated
            # If we are looking for an invalid response, artificially break the valid response by making it too long.
            else:
                return f'{generated}{generated}{generated}{generated}{generated}'
        # If the min and max lengths are incorrect.
        else:
            # If we were hoping for a valid response, take part of the response up until the max length.
            if should_meet_requirements:
                return generated[:max_length]
            # If an invalid response is what we were hoping for, simply return the invalid response.
            else:
                return generated

def locate_element():
    y_cord = random.randint(0, 947)
    return get_element_by_position(90, y_cord)

def register():
    driver.switch_to.window(driver.window_handles[len(driver.window_handles)-1])
    WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.NAME, element_to_wait_on_for_process_launch)))
    selec = get_element_by_position(90,325)
    select_option(selec,1)

    include_regex_js_lib()

    for i in range(20):
        element = locate_element()

        while type(element) == NoneType or type(element.get_dom_attribute('type')) == NoneType:
            element = locate_element()

       # component_data = get_component_data(element)
        # print(element.get_dom_attribute('type'))
        #print(component_data)
        # generated_input = get_text_from_validation_requirements(component_data, ElementActions.FillWithValidText)
        # fill_text(element,  generated_input)

launch_registration()
register()
