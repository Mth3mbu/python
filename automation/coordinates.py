from selenium.webdriver.support.ui import Select
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

element_to_wait_on_for_process_launch = 'dumyr'
service = Service(ChromeDriverManager().install())

observation_width = 670
observation_height = 947
field_position = {'x': 96, 'y': 159}

chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument(f'window-size={observation_width},{observation_height}')
driver = webdriver.Chrome(service=service, options=chrome_options)

driver.get("https://swift3.valueactive.eu/Swift.StateMachine/swiftlaunch/uilauncher")

element = driver.execute_script('return document.elementFromPoint(arguments[0], arguments[1]);', field_position['x'], field_position['y'])

element.clear()
element.send_keys('test data')

WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.NAME, element_to_wait_on_for_process_launch)))


