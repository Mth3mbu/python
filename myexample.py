from distutils.file_util import write_file

write_file('text.txt', ['Jack', 'Mthembu'])

with open('text.txt') as my_file:
    print(my_file.read())
    my_file.seek(0)
    print(my_file.read())
    my_file.seek(0)
    print(my_file.readlines())
    my_file.close()

val = False
if val:
    print('Its true')
else:
    print('its false')

print('______________________________________________')

list = [1, 2, 3, 4, 4, 5, 10]
sum = 0
for value in list:
    sum += value
    if(value % 2 == 0):
        print(value)
    else:
        print('Odd number')
print('______________________________________________')
print(sum)
print('______________________________________________')
tuples_list = [(1, 2), (3, 4), (5, 6), (7, 8), (9, 10)]
for item in tuples_list:
    print(item)

print('______________________________________________')

count = 0
while count < 10:
    print(count)
    count = count+1
else:
    print('counter is greater than 9')

print('______________________________________________')

for i in range(len(list)):
    print(i)
