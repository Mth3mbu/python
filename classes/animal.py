class Animal():
    def __init__(self, name):
        self.name = name

    def walk(self):
        print('{} is walking'.format(self.name))

    def eat(self):
        print('{} is eating'.format(self.name))

class Dog(Animal):
    def __init__(self, name):
        super().__init__(name)
        print('Dog created')

    def walk(self):
        print('Dog named {} is eating'.format(self.name))

    def jump(self):
        print('Dog is jumping')

dog = Dog(name='bhabsi')
dog.walk()
dog.eat()
dog.jump()
