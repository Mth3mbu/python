def first_function(name):
    print(f'function created by {name}')


first_function("Jackaranda")


def add(num1, num2):
    return num1+num2


def player_guess():
    guess = ''
    while guess not in ['1', '2', '3']:
        guess = input('Pick a number between 1,2 and 3')
    return guess


# sum = add(1, 3)
player_guess()
# print(sum)
